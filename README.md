# blog

主题基于： [hugo-theme-dream](https://g1eny0ung.github.io/hugo-theme-dream)

博主是个前端菜鸟，以粗暴的方式对本博客主题进行了魔改（涉及修改主题模板 `themes/dream/`）

- 拓展图标支持阿里图标
- 拓展配置支持修改js、css的cdn地址
- 支持在配置文件中指定首页彩色标签渲染的对象是`标签`还是`分类`
- 左上角、归档等图标后增加文本说明
