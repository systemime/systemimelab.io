---
title: 关于我
date: 2019-11-11T01:30:25+08:00
lastmod: 2022-02-01T17:37:24+08:00
---

systemime 的博客

记录一些生活与技术的事或思考

毕业于 🏫 山东科技大学泰山科技学院

目前职位为Python后端开发工程师

热爱代码，热爱开源

#### 主要的技术栈是：

- python
- celery
- django
- shell
- sql
- go
- nginx
- ...

#### 爱好

- 羽毛球
- 编码
