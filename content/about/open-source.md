---
title: 我的一些开源项目
date: 2019-11-11T01:30:24+08:00
lastmod: 2019-11-11T01:30:24+08:00
---


计划或项目：

- [skill_test](https://github.com/systemime/skill_test) :arrow_right: 一个包含项目常用的django模板：常用脚本、单测方法、数据库连接池、异步请求池，restful风格的回调接口管理器 60%
- [Vbox](https://github.com/systemime/Vbox) :arrow_right: 一个基于k8s和docker的容器云平台，早期项目代码较简单 90%
- [YuQue-Assistant ](https://github.com/systemime/YuQue-Assistant ) :arrow_right: 用于批量拉取语雀工作区文章，使用进程池+协程
- 一个代理池 60%
- [simple_db_pool](https://github.com/systemime/simple_db_pool) :arrow_right: 一个简单数据库连接池 100%
- 一个电报消息转发脚本 90%
- 使用flutter做一个app 计划中
- 其他若干脚本（bilibili、微博图片视频下载、文件对比、图片颜色提取...）
