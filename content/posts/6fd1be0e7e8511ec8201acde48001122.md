---
title: 【Leetcode】两数之和
date: 2022-01-26T16:53:55+08:00
lastmod: 2022-01-26T16:53:55+08:00
author: systemime
avatar: /me/is_me.png
cover: /Gallery/11.webp
categories:
  - 算法
tags:
  - Leetcode
  - 算法
---

<!--more-->

## 描述

```
# 给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target 的那 两个 整数，并返回它们的数组下标。 
# 
#  你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。 
# 
#  你可以按任意顺序返回答案。 
# 
#  
# 
#  示例 1： 
# 
#  
# 输入：nums = [2,7,11,15], target = 9
# 输出：[0,1]
# 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
#  
# 
#  示例 2： 
# 
#  
# 输入：nums = [3,2,4], target = 6
# 输出：[1,2]
#  
# 
#  示例 3： 
# 
#  
# 输入：nums = [3,3], target = 6
# 输出：[0,1]
#  
# 
#  
# 
#  提示： 
# 
#  
#  2 <= nums.length <= 10⁴ 
#  -10⁹ <= nums[i] <= 10⁹ 
#  -10⁹ <= target <= 10⁹ 
#  只会存在一个有效答案 
#  
# 
#  进阶：你可以想出一个时间复杂度小于 O(n²) 的算法吗？ 
#  Related Topics 数组 哈希表 👍 13273 👎 0
```

## 题解

### python-1

以字典记录索引

```
class Solution(object):
    def twoSum(self, nums, target):

        cc = {val: idx for idx, val in enumerate(nums)}

        for val in cc:
            temp = target - val
            if temp in cc:
                if val != temp:
                    return cc[val], cc[temp]
                elif nums.count(val) > 1:
                    idx = nums.index(val)
                    return idx, nums.index(val, idx+1)
```

### python-2

动态添加到数组中优化

```
class Solution(object):
    def twoSum(self, nums, target):

        cc = {}
        for idx, val in enumerate(nums):
            tmp = target - val
            if tmp in cc:
                return cc[tmp], idx
            cc[val] = idx
```

