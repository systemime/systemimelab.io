---
title: Buy Me A Coffee
date: 2018-06-01T00:00:00+08:00
lastmod: 2018-06-01T00:00:00+08:00
author: systemime
avatar: /me/is_me.png
cover: /img/Buy-Me-A-Coffee.jpg
categories:
  - 开源
tags:
  - 赞赏
weight: 1
---

如果你喜欢我的博客、开源项目或者它们可以给你带来帮助，可以赏一杯咖啡 ☕ 给我。~

If you like my open source projects or they can help you. You can buy me a coffee ☕.~

<!--more-->

## 捐赠方式（Ways to Give）

### 微信赞赏码

<img class="ui large image" src="/me/微信赏赞码.jpg" alt="wechat" />

### 支付宝赏赞码

<img class="ui large image" src="/me/支付宝赏赞码.png" alt="alipay" />

### PayPal

[https://paypal.me/systemime](https://paypal.me/systemimeg)

> 最好附加一下信息或者留言，方便我可以将捐助记录 📝 下来，十分感谢 🙏。
>
> It is better to attach some information or leave a message so that I can record the donation 📝, thank you very much 🙏.

## 捐赠列表（Donation List）

暂无信息
