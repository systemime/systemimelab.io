#!/bin/bash

# $1 文件名 $2 文章描述 $3 文章图片 可为空，随机

time=$(date "+%Y-%m-%dT%H:%M:%S+08:00")
img_locl=$3

# 未指定图片文件，获取随机图片
if [ ! "$img_locl" ]
then
    # 图片收集
    img_arr=()
    # for img in `find ./static/Gallery -regextype posix-extended -regex ".*\.(jpg|png|webp|jpeg)" | sed 's#.*/##'`
    for img in `find ./static/Gallery -name "*.webp" -o -name "*.jpg" -o -name "*.png" -o -name "*.jpeg" | sed 's#.*/##'`
    do
        img_arr=(${img_arr[*]} $img);
    done

    # echo "数组的元素为: ${img_arr[*]}"
    img_idx=$((RANDOM%${#img_arr[*]}+1))

    # echo "随机索引: $img_idx"
    img_locl="/Gallery/"${img_arr[$img_idx]}
fi

echo "头图 -> $img_locl"

title_uuid=`python -c 'import sys,uuid; sys.stdout.write(uuid.uuid1().hex)'`

echo "文章目录 -> content/posts/$title_uuid.md"

cat > ./content/posts/$title_uuid.md << EOF
---
title: $1
date: $time
lastmod: $time
author: systemime
avatar: /me/is_me.png
cover: $img_locl
categories:
  - 
tags:
  - 
---

$2

<!--more-->
EOF

echo "$title_uuid::$1" >> blog_index.txt

# xxx
# path=$1

# todays=`date +%s`
# arr=()

# for file in `ls $path`
# do
#     # if [ -d $file ]
#     if test -d $path"/"$file
#     then
#         echo $file
#         dayDiff=`echo "scale=2;($todays-$(date -d "$file" +%s))/86400"|bc`;
#         if [ $(echo "$dayDiff > 30" | bc) = 1 ]
#         then
#             arr=(${arr[*]} $file);
#         fi
#     fi
# done

# # 输出目录
# echo
# echo  ${arr[@]}
# echo


# var="tar -cf lastmoth.tar.gz "
# for i in ${arr[@]};
# do
#     var="$var $path/$i"
# done

# echo $var
# `$var`