"""
pip install Pillow
"""
import logging
import random
from pathlib import Path
from PIL import Image

logger = logging.getLogger(__name__)

img_path = Path(__file__).parent / "static/Gallery"


if __name__ == "__main__":

    img_list = [val for val in img_path.iterdir() if val.stem]
    random.shuffle(img_list)

    for idx, item in enumerate(img_list):
        target = f"{str(item.parent)}/{idx}"
        if item.suffix != ".webp" and item.name != ".DS_Store":
            target = target + ".webp"
            logger.warning(target)
            img = Image.open(item).convert("RGB")
            img.save(target, "WEBP")
            item.unlink()
        else:
            target = target + f"{item.suffix}"
            logger.warning(target)
            item.rename(target)
